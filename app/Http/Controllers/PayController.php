<?php

namespace App\Http\Controllers;


use Culqi\Culqi;
use Illuminate\Http\Request;

class PayController extends Controller
{


    public function createCharge (Request $request)
    {
        $amount = $request->input('amount');
        $token = $request->input('token');
        $culqi = new  Culqi(array('api_key' => env('CULQI_KEY')));

        $charge = $culqi->Charges->create(
            array(
                "amount" => $amount,
                "capture" => true,
                "currency_code" => "PEN",
                "description" => "Venta de prueba",
                "email" => "test@culqiTest.com",
                "installments" => 0,
                "antifraud_details" => array(
                    "address" => "Av. Lima 123",
                    "address_city" => "LIMA",
                    "country_code" => "PE",
                    "first_name" => "Will",
                    "last_name" => "Muro",
                    "phone_number" => "9889678986",
                ),
                "source_id" => $token
            )
        );
        $charge = (array)$charge;
        return response()->json($charge);

    }
}